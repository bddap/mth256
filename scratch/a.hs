
main = interact f
  where
    f _ = show g ++ " " ++ (show h) ++ "\n"
    g = sum $ take 10000 as
    h = 2 * pi / 3

a n = (pi / 2) * 1 / (4 ^ n)

as = map a [0..]
