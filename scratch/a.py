#!/usr/bin/env python3
from itertools import count

def f(n):
    return (-1 ** (n-1)) / (n ** 2 + 9)

def nine(t):
    for n in range(1, t+1):
        yield f(n)

for i in range(1, 500):
    print(sum(nine(i)))
