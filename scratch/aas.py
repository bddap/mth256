#!/usr/bin/env python3

def aas():
    a = 0
    yield a
    while True:
        a = (1+a)*0.5
        yield a

aa = aas()
for i in range(10):
    print(next(aa))
    
