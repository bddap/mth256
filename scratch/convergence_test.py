#!/usr/bin/env python3

def q(s):
    return 'y' in input(s + ' ').lower()

def select(*options):
    for i, n in enumerate(options):
        print(i, ':', n)
    i = input(': ')
    try:
        return options[int(i)]
    except:
        return options[0]
        

approaches_zero = q('Does lim a_n approach 0?')

if not approaches_zero:
    print('Diverges')
else:
    typ = select('Harmonic', 'Geometric', 'p-series', 'Alternating')
    if typ == 'Alternating':
        if approaches_zero and q('Is |a_n| monotonically decreasing?'):
            print('Conditionally convergent.')
    else:
        # Integral convergence test
        if q('Is the integral from 1->inf a_x convergent?'):
            print("Converges")
        else:
            print("Maybe converges")
